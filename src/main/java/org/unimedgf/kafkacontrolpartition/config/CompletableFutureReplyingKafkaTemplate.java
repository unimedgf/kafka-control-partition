package org.unimedgf.kafkacontrolpartition.config;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.GenericMessageListenerContainer;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.concurrent.CompletableFuture;

public class CompletableFutureReplyingKafkaTemplate<K, V, R> extends PartitionAwareReplyingKafkaTemplate<K, V, R> implements CompletableFutureReplyingKafkaOperations<K, V, R> {
    public CompletableFutureReplyingKafkaTemplate(ProducerFactory<K, V> producerFactory, GenericMessageListenerContainer<K, R> replyContainer) {
        super(producerFactory, replyContainer);
    }

    public CompletableFuture<R> requestReply(ProducerRecord<K, V> record) {
        return adapt(doSendAndReceive(record));
    }

    private CompletableFuture<R> adapt(RequestReplyFuture<K, V, R> requestReplyFuture) {
        CompletableFuture<R> completableResult = new CompletableFuture<R>() {
            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                boolean result = requestReplyFuture.cancel(mayInterruptIfRunning);
                super.cancel(mayInterruptIfRunning);
                return result;
            }
        };

        requestReplyFuture.getSendFuture().addCallback(new ListenableFutureCallback<SendResult<K, V>>() {
            @Override
            public void onSuccess(SendResult<K, V> sendResult) {
                // NOOP
            }

            @Override
            public void onFailure(Throwable t) {
                completableResult.completeExceptionally(t);
            }
        });

        requestReplyFuture.addCallback(new ListenableFutureCallback<ConsumerRecord<K, R>>() {
            @Override
            public void onSuccess(ConsumerRecord<K, R> result) {
                completableResult.complete(result.value());
            }

            @Override
            public void onFailure(Throwable t) {
                completableResult.completeExceptionally(t);
            }
        });
        return completableResult;
    }
}
